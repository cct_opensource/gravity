
JSON Schema
===========

Copyright 2021 Creative Collisions Technology, LLC
Written by: J. Patrick Farrell

One of the core featues of Gravity is that it's largely based around passing and storing data in JSON forat. The JSON messages and storage files must follow a particular schema for them to be valid and read by the program.

One of the core featues of Gravity is that it's largely based around passing and storing data in JSON format. The JSON messages and storage files must follow a particular schema for them to be valid and read by the program.

We define these schemas within the "schemas" folder and then use a validation module within the code to make sure that all information conforms to the the schema.

JSON Schema Validation
----------------------

To validate a JSON config file from the command line, we can use the jsonschema program.  Any time a new message or command is added to a JSON message, the schema file should be updated.  This allows us to keep track of the format of our messages, and also allows us to validate that messages passing through the system conform to the JSON schema.

	$ jsonschema -i config.json config.schema

JSON Schema Validation Inside of Gravity Python Code
----------------------------------------------------

Gravity uses the 'jsonschema' module to validate json messages against a defined schema file that we have defined.

[JSON Schema Documentation](https://python-jsonschema.readthedocs.io/en/stable/)

To install via pip:

	$ pip install jsonschema

When you wish to use the jsonschema validation inside of the code, you will need to include:

	from jsonschema import validate
