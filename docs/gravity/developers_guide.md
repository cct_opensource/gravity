
Gravity Developers Guide
========================

Copyright 2021 Creative Collisions Technology, LLC
Written by: J. Patrick Farrell

This file is designed to help developers understand how to build and integrate with The Gravity Framework.

This document is meant to guide the developer for how to work with the code, not how to install or run the framework. If you are looking for installation instructions, you can find them [here][docs/gravity/installation.md].

Data Flow
---------

The Gravity Web Framework is designed to handle client requests, a view or data as a response, and return the requested action to the client.

There are a few key components to this workflow.

### Client HTML Request

When a client requests a particular URL, the web browser will send a request to the Gravity Web Server.

There is a bunch of authentication checks that happen first, but will be added to a future version of this documentation. 

After authentication, then the request is routed through a file called GravityRoutes.py.

GravityRoutes.py contains a table that is designed to route the request to the right controller.

Once the controller is selected based on the desired route, the controller function is called. This happens inside of a function called GravityControllers.py.

#### Controller

The controller is where the bulk of the work for the request is done. The first thing that happens is that the controller determines if there needs to be a data look up from the database engine.  If data is required, this controller function is where that data would be fetched from the database.

At this point, the data will be packaged into a 'context' object and sent to the Jinga2 engine.

All of the jinga2 template files can be found inside of the 'views' folder. This folder contains standard gravity interface views and other default package views that have been implemented. 

If you as the developer want to add more views in the future, it is suggested that you add a new sub-folder to this folder so that they remain separate from the standard Gravity views that are included in the mainline open source project. This way you can keep your views separate than changes you must submit back to the open-source mainline branch.

#### Jinga2 Template

When the controller calls the templating engine, we are currently using a standard library called Jinga2. The result of this call will be an HTML string that will be returned to the user.

#### Resulting DOM

The output of the Jinga2 engine will be passed back to client that made the request, typically a brower. This will be then displayed in the DOM.

It is possible that a second round trip to obtain data from the server will be done after this inital DOM request. The second request would be made over AJAX (preferred) or WebSockets.


Adding New Code
===============

This part of the documentation is designed to help you understand how to add new views, new routes, and new API calls to the Gravity Framework.

We will detail out in this document where you should place code that you want to keep separate for your particular application, and where you should place and update code that is part of the open-source project. If you make changes that are generic that improves the project, we request that you submit changes back to the mainline via a pull-request so they can be incorporated into the mainline project for everyone to use.


Adding a new View
-----------------

1. Visit the templates folder `/views` and build a new template view file (you could copy `about.j2`)
2. Visit the controller folder `/gravityApp/app/GravityControllers.py` and look to line 317 and generate a controller that fits your needs
3. In `GravityControllers.py`, visit line 31 to view the routes
4. Restart server
5. Visit `0.0.0.0/<your_additional_page>`