#!/usr/bin/env python

from GravityCharge import *
from MessageHandlers import *

class MessageHandlersStripe(MessageHandlers):

		def handleStripeCharge(self, messageJson, logger):
		"""
		Handles a Stripe Charge request.
		"""

		command = COMMAND_STRIPE_CHARGE
		STRIPE_TOKEN_KEY = "stripe_token"

		message_response = self.json_message.createResponseMessage( command )

		if STRIPE_TOKEN_KEY in messageJson:
			chargeResult = GravityCharge.stripeCharge( messageJson[STRIPE_TOKEN_KEY] )
			message_response["result"] = chargeResult["status"]
		else:
			reason = "Failed stripe charge because %s is not present in json message" % STRIPE_TOKEN_KEY
			logger.error( reason )
			message_response = self.json_message.createErrorMessage( command, reason )

		return message_response

	def handleStripeGetKeys(self, messageJson, logger):
		"""
		This function returns the keys needed for stripe to function to the user.
		Returns the Stripe public key that is stored in our .env file
		"""

		command = COMMAND_STRIPE_GET_KEYS
		STRIPE_PK_KEY = "STRIPE_PK"

		message_response = self.json_message.createResponseMessage( command )

		try:
			message_response["stripe_pk_key"] = os.getenv("STRIPE_PK")

		except:
			reason = "Failed to return stripe keys because %s is missing from the environment" % STRIPE_PK_KEY
			logger.error( reason )
			message_response = self.json_message.createErrorMessage( command, reason )

		return message_response

	def handleStripeGetSession(self, messageJson, logger):
		"""
		Returns the stripe session information used for Stripe Checkout, which is sent from the javascript on
		the client.

		"""

		command = COMMAND_STRIPE_GET_SESSION

		message_response = self.json_message.createResponseMessage( command )

		if "stripe_sku" in messageJson:
			sku = messageJson["stripe_sku"]

			result = GravityCharge.stripeCheckout(sku)

			message_response["session"] = result

			return message_response

		else:
			message_response = self.json_message.createErrorMessage( MSG_TYPE_REPLY, command, "Could not find strip_sku" )
			return message_response