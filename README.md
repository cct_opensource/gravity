
Gravity Web Server and Framework
================================

Copyright 2025 EIM Global Solutions, LLC
Written by: J. Patrick Farrell

**Live Demo**: https://thegravityframework.com

The Gravity Web Framework is an python based open-source web application framework that is designed to bring developers around world working to work together on a core web framework platform, hence the name "Gravity". Gravity is designed to become an "app in a box" making it easier to build an app without having to worry about all the foundational components.

Gravity is a boilerplate framework to build backend database-driven software applications. If you have ever tried in the past to build a web or backend mobile application from scratch, you know how difficult it can be to find a framework that gives you everything you need to get started quickly. 

Gravity was started out of a personal need Patrick Farrell needed for his own projects and expanded into this full web framework. Patrick realize there were many core features that most developers and organizations would need when they were developing their applications, and Gravity is intended to incorporate many of those core features into the open-source packaage. As the developer community grows, the intention much more support to be incorporated into the baseline framework and also future plug-in modules.

Gravity is a full-stack framework, which means it consists of all the components a developer will need to build a fully functional web application. Gravity is also designed to be the backend framework for a mobile app that communicates over a JSON based Application Programmer Interface (API).

This is a pure python implementation of a web server.  This is an open-source framework that provides a baseline implementation with core features such as user-login, JSON Web Tokens (JWT), session management (via JWT and cookies), and a JSON API.

You can run this web server on your host operating system with just python, but it is also possible to run this web server inside of a Docker container. When running as a docker container, this makes the entire system much easier to manage and deploy across differerent machines and environments.

The ultimate goal of Gravity is to make web and mobile application development simplier and to accelerate the development cycle of start-up companies, universities, and other organizations development from concept to working product. We believe that based on the features incorporated into Gravity, someone could cut several months (if not years in the future as the project grows), off their development cycle.

Working on Gravity? [Join the conversation](https://gravityserver.slack.com).

Features
--------

* Server-Side Rendering (SSR) with using Jinga2 templating engine
* Full Docker Containerization Implementation
* Model, View, Controller Architecture for ease of coding and implementation
* Built-in JSON Application Programmer Interface (API) via AJAX or Websockets
* Local Authentication using Email and Password
* JSON Web Token (JWT) Authentication Support
* Bootstrap 4 Web and Mobile Theme
* Message Schema authentication using jsonschema
* File Upload over HTTP POST
* Complete Offline Support Framework for stand-alone implmentation on embedded systems.
* Stripe Payment Processing and Checkout Support
* Administrator Backend Interface for User Management
* Administrator Backend Interface for Blog Management (present but core features in-development)
* Administrator Backend Interface for Event Management (present but core features in-development)

Prerequisites
-------------

* Python 3.12
* Command Line Tools
* Docker Installation
* Python Dependencies defined in the OS specific implemenation, follow [setup folder](docs/python)

Documentation Examples
----------------------

While not yet complete, the goal for this documentation is to be an extremely complete and evolving documentation framework to build, develop, and deploy The Gravity Framework in multiple types of environments.  This is going to require a large effort to make sure we develop this documentation in a way that is easy to understand and complete.

[Documentation Example](http://www.davidketcheson.info/2015/05/13/add_a_readme.html)

Developers for Humanity Agreement
---------------------------------

This project is an open source project, and therefore is considered #softwareforhumanity.  This project has strong principles to bring people together, to educate, and to build a baseline web framework for the organizations to work together on projects.  Please read the developers documentation before you begin contributing to The Gravity Framework.

This documentation describes the basic principles behind how we work together, how we structure and develop the code, and how we can support the community.

[Developers For Humanity](docs/gravity/developersforhumanity.md)

Build Dependencies
------------------

Gravity is a very versatile framework, capabile of running on Linux, MacOS and probably Windows (under Docker, though I haven't tried).  

The easiest way to build and deploy is with Docker. We also support running with a standard python environment. Follow the initial setup instructions to get started installing the build dependencies based on your host OS and your deployment environment (python or Docker).

Build Procedure
---------------

There are a couple ways to build and run this project. I would recommend starting with Docker and if you need to run it Natively on the host machine you can as an option.

1) You can build the program into a docker image and run with Docker. 
2) You can run it with the python interpreter directly without compiling.
2) You can compile the program into .pyc files and run them with the python interpreter.  This will require all of the necessary .pyc files for all of the classes to run.

IMPORTANT: When running with Docker, you MUST build the base image FIRST before you build the gravity image. The base image contains the operating system and dependencies so you don't have to build that every time when you are developing your source code with Gravity.

Once you have your environment setup, building an running is as simple as:

	$ make build
	$ make image-run

Find more detailed instructions to get Gravity working with Docker in the following documentation.

[Docker Build](docs/docker/gravity_docker.md)

Otherwise if you want to build and run natively, follow this documentation.

[Python Build](docs/python/gravity_python.md)

Run Procedure
-------------

### Docker Run Instructions

For instructions on how to run with Docker, follow the instructions within the Gravity Docker file.

[Gravity Docker](docs/docker/gravity_docker.md)

### Run with python interpreter directly.

When you run with the python interpreter directly, there is no build or compile necessary.  However, your host machine must have all of the python dependencies installed as mentioned above.

	$ cd gravityApp/app
	$ python GravityServer.py

Installing and Running in Production
------------------------------------

[Gravity Installation](docs/gravity/installation.md)

When installing for production, all the configuration and run files are located in /usr/local/gravity.

When we are running the Gravity system in production, we should be using an Nginx Front-End. We have designed the cct_nginx docker image to be run as this front-end web server.  Nginx handles multiple domains on a single server and is also necessary to be a robust web server that is necessary on the global interet.

Production is designed to run with docker. This is why the docker image only exposes the HTTPS port, port 80 internally. This means that in production, the Nginx front-end handles all of the SSL configuration and certificates.  Then is passes traffic locally to another port that gravity is configure to run on. By default we have configured the gravity-compose.yml to use port 3001, but you can change this port if desired. Ensure that your server does not expose this port to the outside world.

You can find more information about configuring and running the Ngninx Front End web server in the documentation folder.

Project Structure
-----------------

| Name                                       | Description                                               |
| -------------------------------------------| ------------------------------------------------------    |
| **client**                                 | Contains client-side programs that interface with Gravity |
| **data**                                   | Contains data stored for the Gravity Server Side          |
| **docs**                                   | Contains Gravity Documentation.                           |
| **etc**                                    | Linux configuration files.                                |
| **gravityApp/app**/FileTools.py            | Helper class that handles working with files              |
| **gravityApp/app**/GravityBlog.py          | File that controls the Gravity Blogging Engine            |
| **gravityApp/app**/GravityCharge.py        | Helper class that works with payment functions            |
| **gravityApp/app**/GravityConfiguration.py | Handles all configuration of Gravity with database files. |
| **gravityApp/app**/GravityControllers.py   | Contains the web controllers that generate views          |
| **gravityApp/app**/GravityContacts.py      | Class that handles working Gravity Contact Engine         |
| **gravityApp/app**/GravityDocker.py        | Handles launching and working with other Docker containers|
| **gravityApp/app**/GravityEvents.py        | Functions that handle and work with Event Management      |
| **gravityApp/app**/GravityMain.py          | Main Entry Point into the program.                        |
| **gravityApp/app**/GravityRoutes.py        | Determines the controller to use                          |
| **gravityApp/app**/GravityServer.py        | Class that creates the web server                         |
| **gravityApp/app**/GravityTime.py          | Class that helps with time management functions           |
| **gravityApp/app**/HTTPWebHandler.py       | Class that works with the web server to handle requests.  |
| **gravityApp/app**/JsonMessage.py          | Message helper class for JSON Messages                    |
| **gravityApp/app**/JsonSchemaHelper.py     | Helper class for message schema validation                |
| **gravityApp/app**/LogHelper.py            | Class that helps with logging for debugging               |
| **gravityApp/app**/MessageHandlers.py      | Message handler functions for processing requests         |
| **gravityApp/app**/MessageParser.py        | Class that handles parsing JSON Messages                  |
| **gravityApp/app**/SystemTools.py          | Tools that work with the host operating system (Linux)    |
| **gravityApp/app**/UserManagement.py       | Functions that handle user creation, deletion, etc        |
| **models**                                 | Contains static Gravity data for views.                   |
| **public**/*                               | Contains default HTML, CSS and JS files                   |
| **schema**/*                               | Contains the schema files for JSON validation             |
| **scripts**/*                              | Contains linux configuration scripts                      |
| **security**/*                             | Main contain default security configuration files         |
| **views/gravity**/*                        | These are the the view templates included with Gravity    |
| **views/stripe**/*                         | These are the the view templates associated with Stripe   |
| Dockerfile                                 | File that defines how Docker container is built           |
| gravity-compose.yml                        | Defines how Gravity is deployed with Docker Compose       |
| Makefile                                   | File that easily builds and deploys Gravity Framework     |


Note:

All Gravity python files should currently be placed in **gravityApp/app**.

If you are developing your own application on top of Gravity, it is recommended that you create a separate
**views**/<your applcation> folder so that your files are separate from any baseline Gravity files.