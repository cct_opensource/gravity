
Python Documentation and Notes
==============================

Copyright 2021 Creative Collisions Technology, LLC
Written by: J. Patrick Farrell

Python is the software technology used with Gravity. This document is intended to show the general
notes and documentation information when using python with gravity.

Python PIP - Module Installation
--------------------------------

We use python pip to install standard modules from the pip library.


Sometimes you might want to download a python pip module that can be installed offline.

    $ mkdir <package-name>_install_files
    $ cd <package-name>_install_files
    $ pip download <package-name>

You will end up with both the package files that are for the package you wanted to download, and any dependency files that are associated with that package.

Note: These files might be architecture dependent. So if you download them on Mac OS, they might not work on Linux. And if you download them on a x86 processor, they might not work on an ARM processor. So be sure to check that the files you download install correctly when installed on the appropriate systems.

Now once you have the package file, if you wish to install the python package from the package file, you would do the following. Be sure to install all of the files in the correct order.

    $ pip --isolated install <filename>


### Install into Virtual Environment

If you wish to test this out in a virtual environment before installing these files onto your system.

    $ virtualenv -p python3 testproject
    $ cd testproject
    $ source bin/activate

