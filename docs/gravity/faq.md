
Frequently Asked Questions (FAQ)
================================

Copyright 2021 Creative Collisions Technology, LLC
Written by: J. Patrick Farrell

This document houses all of our frequently asked questions about how to design, develop and work with Gravity.

### Why did you build The Gravity Framework?

This framework was built because there was a need for a platform to easily build web frameworks both for online systems and embedded offline systems.

While Gravity can of course be run on the traditional internet, a unique feature of this framework over many others is that it was also design to run offline. So think about networks such as cruise ships, airplanes, and wireless networks on islands without a solid connection to the global internet.

We also wanted to accelerate development time for a start-up to go from idea to working MVP and eventually product.

### Why did you use python?

We used python because it is an easy to understand langugage, is under active development, and is also a synchronous blocking framework.

Since Gravity is often used in embedded systems where calls need to be blocking (or synchronous), frameworks such as Node.js that offer an asynchronous callback structure didn't work for many of our applications.

This is why we decided to take inspiration from other Node.js frameworks, but build this framework completely from scratch