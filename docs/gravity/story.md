
The Gravity Framework Story
===========================

Copyright 2021 Creative Collisions Technology, LLC
Written by: J. Patrick Farrell

Gravity is a python based open-source web application framework that was created out of my own need and desire for a boilerplate framework to build backend database-driven software applications. What I was looking for at the time was a framework that would provide a baseline opensource software so that I could modify and add code for several client projects I was working on.

At the time, I was being hired by a couple start-up companies that were looking to create a backend software framework that provided relatively basic functionality that is available on most websites on the internet. 

Surprisingly though, it was difficult to find a framework to start from. I needed a framework that included user login, the ability to interact with a database, and a structure to build on top of and modify the source code.

With the abundance of software applications on the internet, I would have thought something like this would be pretty common, however I was incorrect. It was actually very challenging to find something that wasn’t super complex and that was extensible to build my own application on top of. I was also looking for a project that was actively maintained by the community.

I ran into multiple issues along the way when I started searching for platforms and some of the ones I found at the time were:

 - Web2py
 - Megaboilerplate

What I also found was that many of the boilerplate frameworks that were out there were built on Node.js. However, I was running into some issues when using a Javascript based framework. 

Javascript is inherently asynchronous, which means that I would be sending a command off and be waiting for a callback. This works for straight web applications, but I was also looking to use Gravity in some embedded systems projects and this was not working for me. I needed a synchronous framework with blocking calls.

So after searching far and wide, and not finding anything I truly liked, I decided that I needed to build my own framework.

I needed something that offered a web server, an Application Programmer Interface (API), framework for interaction with databases, schema data validation, and that was also easy to expand on top of.

I also wanted it to work on a native linux environment or run in a Dockerized container environment. The docker environment was something important as I wanted to create different containers that accomplished different tasks so not everything was embedded in the web container.

By containerizing the application, this would allow applications to be developed that performed operations such as photo organization, data manipulation, or data processing without the need for the front end. This would allow developers to work in parallel much easier and then the Gravity front-end could be more for display. The intention was to keep a pure Model, View, Controller framework when building Gravity.


Gravity was designed to have all of the basic features that would be necessary for any start-up company to create a first minimum viable product on top of gravity quickly, and then extend that functionality over time so.

The idea for this framework too is to build a strong open source community around Gravity, that way as the project progresses and incorporates more basic features into the project, everyone wins.  

Let’s say for instance a developer decides that he wants to integrate Gravity with a new type of database, say Postgres which isn’t currently implmentened. If this developer worked on the project and made postgress work in his project, then submitted that code back to the mainline, all other users of Gravity would benefit and could then use this same code.

This is the intention. To give companies, organizations, and individual developers a place to start. A place where they can get a website up and running quickly. A platform where they can easily learn how to send data back and forth between the client and the server.

Gravity includes a JSON API that makes it easy to send data in and out of the server. Because gravity is mostly built on Javascript and Python, the JSON format makes it easy to convert and send data back and forth.


Gravity was also built with the intention of working offline first. Think of situations such as a cruise ship or a MANET network where access to the global internet is unavailable. Gravity is designed and actually thrives in this environment. No other open source boilerplate web framework out there is designed to work offline first. Of course it will work “online” but when we develop for offline first, we have a lot more flexibility and control.

Gravity is also very light in terms of dependencies by design. We don’t want the framework to require hundreds of dependencies as that can cause upgrade and maintenance issues. The framework is designed with a minimalist approach and if additional libraries are necessary they can be added in a modular format or with other docker containers to separate out the processing. 

Requirements:

* Baseline boilerplate framework
* Easy to understand framework
* Built as a teaching framework
* Built In Application Programmer Interface (API)
* User Login with JSON Web Token (JWT) Authentication built-in
* Server-side HTML templating
* AJAX and Websocket support with JWT authentication
* Model, View, Controller architecture
* SSL Authentication
* User Profiles with user settings
* Built with Docker containerization and integration
* Message Schema authentication
* Minimal Dependencies
* Work in an offline environment without the direct for external web services




