
Gravity Architecture
====================

Copyright 2021 Creative Collisions Technology, LLC
Written by: J. Patrick Farrell

Main Program
------------

GravityServer.py is the main program that runs the Gravity Web Server. This program can be the main entry
point into the system by running:

	$ python GravityServer.py

You also have the option though to run GravityMain.py which launches a thread where the GravityServer runs. GravityMain is currently only setup to run as an HTTP server, using this you would have an Nginx web server front-end.

The reason you would start here is that then you can launch other programs as sub-threads within the main program. It utilizes a GravityServerHelper class that you can copy and modify for a new service that you can build to run in parallel with the Gravity Server.

	$ python GravityMain.py

This is helpful when you have other services that should work along side Gravity acting as the same program. This type of architecture is mainly used when we are NOT using Docker.

If you are using Docker, ideally these other programs that are running as threads in this architecture actually would be run inside of another Docker image. Then you would use ways for the running Docker containers to communicate with each other. For more information on this you can look into the [Gravity Docker Documentation](docs/gravity_docker.md)

Gravity follows a Model, View, Controller Architecture
------------------------------------------------------

Gravity has been separated into a Model, View, Controller architecture. The following describes the operation and how the architecture works.

Request Comes in
Determines the Route
Route is decided which controller should handle the request.
The controller determines if there is data that is necessary to be requested and processed.

Any data is then sent to the view, which is rendered by the Jinga2 templating engine.

The output of the templating engine is sent back to the client as straight HTML.

There might be a second round request through AJAX or Websockets.

